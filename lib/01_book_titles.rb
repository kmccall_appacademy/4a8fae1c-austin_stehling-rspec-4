class Book
  LOWER_CASE = ["the", "a", "an","in", "of", "and"]
  attr_reader :title

  def title=(title)
    title = title.split(" ")

    title = title.map.each_with_index do |word, i|
      LOWER_CASE.include?(word) && i != 0 ? word.downcase : word.capitalize
    end

    @title = title.join(" ")
  end
end
