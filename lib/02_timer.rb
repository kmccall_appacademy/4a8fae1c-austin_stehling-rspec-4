class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def zero(num)
    num > 10 ? "#{num}" : "0#{num}"
  end

  def sec
    seconds % 60
  end

  def min
    (seconds % 3600) / 60
  end

  def hour
    seconds / 3600
  end

  def time_string
    "#{zero(hour)}:#{zero(min)}:#{zero(sec)}"
  end

end 
