class Dictionary

  def initialize
    @d = {}
  end

  def entries
    @d
  end

  def add(entry)
    if entry.is_a?(String)
      @d[entry] = nil
    else
      entry.each do |key, value|
        @d[key] = value
      end
    end
  end

  def keywords
    @d.keys.sort
  end

  def include?(entry)
    @d.has_key?(entry)
  end

  def find(entry)
    found = Hash.new
    @d.each do |key, value|
      if key == entry
        found[key] = value
      elsif key.include?(entry)
        found[key] = value
      end
    end
    found
  end

  def printable
    entries = keywords.map do |keyword|
      %Q{[#{keyword}] "#{@d[keyword]}"}
    end

    entries.join("\n")
  end
end
